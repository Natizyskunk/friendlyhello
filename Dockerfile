# Use an official Python runtime as a parent image
FROM python:2.7-slim

# Install any needed packages for the container.
RUN apt update -y && apt upgrade -y && apt dist-upgrade -y
RUN pip install Flask
RUN pip install Redis
#  RUN apt-get install -qy python3
#  RUN apt-get install -qy python3-pip

# Set the working directory to /app
#  ADD . /myapp
#  WORKDIR /myapp
#  ADD . /app
WORKDIR /app

# Copy the current directory contents into the container at /app
#  COPY . /myapp
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt
#  RUN pip-3.3 install -r requirements.txt
#  RUN pip-3.3 install .

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Set proxy server, replace host:port with values for your servers
#  ENV http_proxy host:port
#  ENV https_proxy host:port

# Run app.py when the container launches
CMD ["python", "app.py"]
## CMD myapp --port 8000